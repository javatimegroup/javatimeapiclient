package br.eti.cvm.javatimeapiclient.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Data;

@Data
@JacksonXmlRootElement(localName="DateTimeDto")
public class DateTimeDto {
	
	private LocalDate date;
	
	private LocalTime time;
	
	private LocalDateTime datetime;
	
}
