package br.eti.cvm.javatimeapiclient.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.eti.cvm.javatimeapiclient.dto.DateTimeDto;
import br.eti.cvm.javatimeapiclient.service.RestClientService;

@Controller
@RequestMapping({"","/"})
public class IndexController {
	
	@Autowired
	private RestClientService restClientService;
	
	@GetMapping
	public String getDateTime(Model model) {
		DateTimeDto dto = restClientService.getDateTimeDto(MediaType.APPLICATION_JSON);
		
		model.addAttribute("data", dto);
		
		return "index";
	}
}
