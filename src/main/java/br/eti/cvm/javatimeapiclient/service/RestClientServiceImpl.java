package br.eti.cvm.javatimeapiclient.service;

import java.util.Collections;

import javax.annotation.PostConstruct;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.eti.cvm.javatimeapiclient.dto.DateTimeDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
@Service
public class RestClientServiceImpl implements RestClientService {

	private final RestTemplateBuilder restTemplateBuilder;
	
	private RestTemplate restTemplate;
	
	private final String API_URI = "http://localhost:8080";
	
	@PostConstruct
	private void init() {
		restTemplate = restTemplateBuilder.build();
	}
	
	
	@Override
	public DateTimeDto getDateTimeDto(MediaType mediaType) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(mediaType));
		
		HttpEntity<DateTimeDto> entity = new HttpEntity<>(headers);
		
		ResponseEntity<DateTimeDto> response = restTemplate.exchange(API_URI, HttpMethod.GET, entity, DateTimeDto.class);
		
		DateTimeDto dto = response.getBody();
		
		log.info(dto.toString());
		
		return dto;
	}

}
