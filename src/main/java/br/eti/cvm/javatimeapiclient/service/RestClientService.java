package br.eti.cvm.javatimeapiclient.service;

import org.springframework.http.MediaType;

import br.eti.cvm.javatimeapiclient.dto.DateTimeDto;

public interface RestClientService {
	DateTimeDto getDateTimeDto(MediaType mediaType);
}
