package br.eti.cvm.javatimeapiclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavatimeapiclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavatimeapiclientApplication.class, args);
	}

}
